module SessionsHelper
    # Loga o usuario pedido.
    def log_in(user)
        session[:user_id] = user.id
    end
  
    # Retorna o usuario atual logado (se existir).
    def current_user
        @current_user ||= User.find_by(id: session[:user_id])
    end
    
    # Retorna true se o usuario estiver logado e falso se for ao contrário.
    def logged_in?
        !current_user.nil?
    end

    # Mostra um alerta de acordo se o usuario estiver logado.
    def logged_user
        respond_to do |format| 
            format.html { redirect_to current_user, notice: "Já está logado!"}
        end if logged_in?
    end

    #Verifica se existe alguem logado, se não tiver redireciona para o login.
    def non_logged_user
        respond_to do |format|
            format.html { redirect_to login_path }
            flash[:info] = "Por favor, faça login."
        end if !logged_in?
    end
    
    # Deslogado o usuario atual.
    def log_out
        session.delete(:user_id)
        @current_user = nil
    end
end
