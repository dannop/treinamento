class User < ApplicationRecord
    has_secure_password
    has_many :posts, dependent: :destroy
    
    validates :name, presence: true, length: { in: 2..50 }
    validates :email, presence: true, length: { in: 2..100 }
    validates :password_digest, presence: true, uniqueness: true, length: { minimum: 3 }
      
    # Returns the hash digest of the given string.
    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
end
