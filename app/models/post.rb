class Post < ApplicationRecord
  belongs_to :user

  validates :title, presence: true, length: { in: 6..50 }
  validates :content, presence: true, length: { in: 1..140 }
end
